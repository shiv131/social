const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth');
const keys = require('./key.js');

passport.use(
    new GoogleStrategy({
    
    //google start
    callbackURL : '/auth/google/redirect',
    clientID :' keys.google.clientID',
    clientSecret : 'keys.google.clientSecret'

},() => {
    
    //callback
})
)
