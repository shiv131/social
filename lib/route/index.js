//const adminRoute        = require('../module/admin/adminRoute'),
      userRoute         = require('../module/user/userRoute'),
     // groupRoute        = require('../module/group/groupRoute'),
    //  cronjobRoute      = require('../module/cronjob/cronRoute'),
      
      responseHandler   = require('../responseHandler');
      basicAuth         = require('../middleware/basicAuth');

      userauth          = require('../module/user/userauth');
     
//========================== Export Module Start ==========================

module.exports = function(app){
    // Attach User Routes
 
    app.use('/demo/api/v1/user', userRoute);

    app.use('/demo/api/v1/userlogin', userauth);
   

    // Attach ErrorHandler to Handle All Errors
    app.use(responseHandler.handleError);
}
//========================== Export Module End ============================