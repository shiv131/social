const router = require('express').Router();
const passport = require('passport');
// const GoogleStrategy = require('passport-google-oauth');

router.get('/google', passport.authenticate('google',{ scope: ['https://www.googleapis.com/auth/plus.login', 'profile', 'email'] }));

router.get('/auth/google/callback',passport.authenticate('google'), (req, res) => {
    res.send('you reached the redirect URI');
    // res.send(res,"PPPPPPPPPPPPPPPPPPPPPPPPPPP")
    });


    router.get('/auth/facebook', passport.authenticate('facebook'));
  
    router.get('/auth/facebook/callback',
    passport.authenticate('facebook', { successRedirect: '/',
                                      failureRedirect: '/login' }));

module.exports = router;

