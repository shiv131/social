"use strict";
//========================== Load Modules Start =======================

//========================== Load internal modules ====================
var mongoose = require("mongoose");
var promise = require("bluebird");
var passport = require("../user/passport");

var _ = require("lodash");
//========================== Load internal modules ====================
const User = require('./userModel');


// init user dao
let BaseDao = new require('../../dao/baseDao');
const userDao = new BaseDao(User);



//========================== Load Modules End ==============================================

function signUp(userInfo) {

    userInfo.totalPred = 0;
    userInfo.wonPred = 0,
    userInfo.lostPred = 0;

    let user = new User(userInfo);
    return userDao.save(user);
}

function isloginExist(loginInfo){
    let query = {}
    query.email = loginInfo.email;
    query.password = loginInfo.password
    return userDao.findOne(query)
    .then(function(response){
    if(response)
    return response;
    else
    return false;
    })
    }


function isSocialUser(params){
        let query = {};
        query[params.type] = params.social_id;
        console.log(query, "query", params);
        return userDao.findOne(query)
        .then(function(response){
        console.log(response);
        if(response)
        return response;
        else
        return false;
        })
        
   }    

function isloginExist(loginInfo){
        let query = {}
        query.email = loginInfo.email;
        query.password = loginInfo.password
        return userDao.findOne(query)
        .then(function(response){
        if(response)
        return response;
        else
        return false;
        })
    }   

function getUserByDeviceId(loginInfo) {
    let query = {};
    query.status = true;
    query.deviceID = loginInfo.deviceID
    return userDao.findOne(query)
}

function updateUser(query,update) {
        update.updated = new Date();   
    let option = {};
        option.new = true;
    return userDao.findOneAndUpdate(query, update, option);
}

function isEmailIdExist(params) {
    let query = {};
    query.email = params.email;
    return userDao.findOne(query)
        .then(function (result) {
            if (result) {
                return true;
            }
            else {
                return false;
            }
        })
}


function isemailexist(user){
    // console.log(user,"dao");
    
    let query = {}
    query.email = user;
    return userDao.findOne(query)
    .then(function(result){
    console.log(result,"result");
    
    if(result){ 
    console.log("already exist email try with another");
    return false;
    }
    else
    {
    signUp(paramss)
    .then(function(){
    console.log("data inserted");
    })
    console.log(user,'else wala ');
    }
    
    
    })
    }



function isSocialIdExist(params) {
    let query = {};
    if(params.socialType==1){       //1= Facebook, 2=Google
        query.facebookId = params.socialId;
    }else{
        query.googleId = params.socialId;
    }
    
    return userDao.findOne(query)
}

function getUserProfile(userId) {
    let query = {};
    // query.isActive = true;
    query._id = userId
    return userDao.findOne(query)
}

function getOtherUserProfile(params) {
    let query = {};
    // query.isActive = true;
    query._id = params.userId
    return userDao.find(query)
}


function userList(params) {
    let query = {};
    if (params.search) {
        query["$or"] = [{ "name": { $regex: params.search } }]
    }
    if (params.pageNo) {
        let pageNo = params.pageNo - 1;
        let count = parseInt(params.count);
        return User.find(query, '_id name profileImage').lean().sort({ name: 1 }).skip(pageNo * count).limit(count);

    }
    else {
        return User.find(query, '_id name profileImage').lean().sort({ name: 1 })

    }
}

function givePoll(params) {
    let query = {};
    query.appUserID = params.userId;
    let update = {};
    update["$inc"] = { 'totalPred': 1 };
    update.updated = Date.now();
    let option = {};
    option.new = true;
    return userDao.findOneAndUpdate(query, update, option);
}

function pollPrediction(params) {
    let query = {};
    query.appUserID = params.userId;
    let update = {};
    if (params.pollType == 1) {
        update["$wonPred"] = { 'totalPred': 1 };

    } else {
        update["$lostPred"] = { 'totalPred': -1 };

    }
    update.updated = Date.now();
    let option = {};
    option.new = true;
    return userDao.findOneAndUpdate(query, update, option);
}

function getByKey(query) {
    return userDao.findOne(query)
}

//========================== Export Module Start ==============================

module.exports = {
    signUp,
    getUserByDeviceId,
    updateUser,
    isSocialIdExist,
    getByKey,
    
    givePoll,
    pollPrediction,
    isEmailIdExist,
    getUserProfile,
    getOtherUserProfile,
    userList,
    isloginExist,
    isSocialUser,
    isemailexist,
};

//========================== Export Module End ===============================
