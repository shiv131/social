// Importing mongoose
var mongoose = require("mongoose");
var constants = require('../../constant');

/*var rootRef = firebase.database();*/

var Schema = mongoose.Schema;
var User;

var UserSchema = new Schema({
   
        email: {
            type: String,
            index: true,
            unique: true
        },
        password: {
            type: String
        },
        
        name: {
            type: String,
            default: ''
        },
        username: {
            type: String,
            default: ''
        },
        mobile: {
        type: Number,
        },
        facebookId: {
            type : String
            },
        googleId: {
            type : String
            }

        
   
},
);

UserSchema.methods.toJSON = function () {
    var obj = this.toObject();
    delete obj.updated;
    return obj;
};

//Export user module
User = module.exports = mongoose.model(constants.DB_MODEL_REF.USER, UserSchema);

/*module.exports = rootRef*/
